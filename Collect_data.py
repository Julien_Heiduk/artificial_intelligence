import pandas as pd
import requests
import time

def download_file(day,month,key):
    url = "http://www.jl-insight.com/Board/pet/2/" + str(key) + "/full/0/D/" + str(day) + "/0" + str(month) + "/2019/extract?token=eyJpYXQiOjE1NTAyNDYxMzAsImFsZyI6IkhTMjU2IiwiZXhwIjoxMTAxMTA0NjEzMH0.eyJpZCI6Ijg4MmIzYzRjLWU4YTgtNDRjNi1hZjM4LTYyNmMxMjQzZGQ5MSJ9.wSMfuoG6bZ49VJtqJd_93pKFI1Xi1LH2oioTRy2_sFk"
    r = requests.get(url)
    dump = r.content
    df = pd.DataFrame(dump.split('\n'))
    df.columns = ['Data']
    df = df.Data.str.split(';',expand=True)
    return df
    
df = pd.read_excel('Key_data.xlsx')

Months = ['1','2']
Day_jan = range(32)
Day_feb = range(29)

counter = 1

for k in range(len(df['Key'])):
    key = df['Key'][k]
    t = time.time()
    print(key)
    
    for i in Months:
        
        if i == '1':
            for j in Day_jan[1:]:
                if counter == 1:
                    output = download_file(j,i,key)
                    if output.shape != (1,1):
                        counter = counter + 1
                if counter > 1:
                    out_temp = download_file(j,i,key)
                    #print(out_temp)
                    output = pd.concat([output, out_temp])
                    #print(output)
        
        if i == '2':
            for j in Day_feb[1:]:
                    out_temp = download_file(j,i,key)
                    output = pd.concat([output, out_temp])
    elapsed = time.time() - t
    print(elapsed)
    
    output.to_csv('/root/JaggerLewis/artificial_intelligence/Notebooks/output.csv',index=False)
